export default class SchoolGpaController {
  constructor(schoolGpaService) {
    this.schoolGpaService = schoolGpaService;
    this.loading = true;
    this.activeTab = 1;
    this.data = [];
    this.studentsData = [];
    this.studentName = '';
    this.studentGpa = null;
    this.currentNameGrade = '10a';
    this.uniqueGradeName = false;
    this.validGpa = false;
    this.validNameStudent = false;
    this.floatRegex = /^\d*[.]?\d{0,2}$/;
    this.nameRegex = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
    this.averageGpa = {count: 0, sum: 0, average: null};
    this.gridOptions = {
      infiniteScrollRowsFromEnd: 3,
      infiniteScrollUp: true,
      infiniteScrollDown: true,
      columnDefs: [
        {field: 'name', name: 'Name', width: '70%', enableColumnMenu: false},
        {field: 'gpa', name: 'Gpa', width: '20%', enableColumnMenu: false},
        {
          name: ' ', enableColumnMenu: false, enableSorting: false, width: '10%',
          groupingShowAggregationMenu: false, enableCellEdit: false,
          cellTemplate: '<span class="btn-close"><i class="fa fa-close" title="Delete student" ng-click="grid.appScope.$ctrl.deleteStudentRow(row)"></i></span>'
        },
      ],
      data: 'data',
    };
    this.initData();
  }

  initData() {
    this.schoolGpaService.getLocalJson().then((response) => {
      if (response.statusText === 'OK' && response.status === 200 && response.data) {
        this.data = response.data['tabs'];
        const initializeTabs = this.data.find(res => res.active);
        if (initializeTabs) {
          this.studentsData = initializeTabs.students;
          this.gridOptions.data = this.studentsData;
          this.activeTab = initializeTabs.id;
          this.loading = false;
        }
        this.checkUniqueGrade(this.currentNameGrade);
        this.calculateAverageGpa();
      }
    }, (response) => {
      if (response.statusText === 'Not Found' && response.status === 404) {
        this.data = null;
        this.averageGpa = null;
      }
    });
  }

  calculateAverageGpa() {
    this.averageGpa = {count: 0, sum: 0, average: null};
    this.data.forEach((res) => {
      res.students.forEach((resStudents) => {
        if (resStudents['gpa'] && typeof resStudents['gpa'] === 'number') {
          this.averageGpa.count += 1;
          this.averageGpa.sum += resStudents['gpa'];
        }
      })
    });
    if (this.averageGpa.count > 0) {
      this.averageGpa.average = (this.averageGpa.sum / this.averageGpa.count).toFixed(2);
    }
  }

  changeActiveTab(tab) {
    const currentTab = this.data.find(res => res.id === this.activeTab);
    if (currentTab) {
      currentTab.students = this.gridOptions.data;
      currentTab.active = false;
    }
    const newTab = this.data.find(res => res.id === tab.id);
    if (newTab) {
      this.studentsData = newTab.students;
      this.gridOptions.data = this.studentsData;
      this.activeTab = tab.id;
    }
  }

  addTab() {
    if (!this.data.find(res => res.gradeName === this.currentNameGrade)) {
      const currentTab = this.data.find(res => res.id === this.activeTab);
      if (currentTab) {
        currentTab.students = this.gridOptions.data;
        currentTab.active = false;
      }
      this.studentsData = [];
      this.gridOptions.data = this.studentsData;
      this.activeTab = this.data[this.data.length - 1].id + 1;
      this.data.push({
        id: this.activeTab, gradeName: this.currentNameGrade, active: true, students: [],
      });
      this.checkUniqueGrade(this.currentNameGrade);
      if (this.studentName !== null) {
        this.checkNameStudent(this.studentName);
      }
    } else {
      alert('Grade name is not unique');
    }
  }

  removeTab(tab) {
    let indexTab = this.data.indexOf(tab);
    this.data.splice(indexTab, 1);
    if (this.data.length !== 0) {
      if (tab.students.length > 0) {
        this.calculateAverageGpa();
      }
      this.checkUniqueGrade(this.currentNameGrade);
      if (tab.id === this.activeTab) {
        if (!this.data[indexTab]) {
          indexTab -= 1;
        }
        this.data[indexTab].active = true;
        this.studentsData = this.data[indexTab].students;
        this.gridOptions.data = this.studentsData;
        this.activeTab = this.data[indexTab].id;
      }
    }
  }

  addStudent(name, gpa) {
    if (!this.studentsData.find(res => res.name === name)) {
      const index = this.studentsData.length === 0 ? 1 : this.studentsData[this.studentsData.length - 1].id + 1;
      this.studentsData.push({id: index, name: name, gpa: +gpa});
      this.logicTableRow();
    } else {
      alert('Student name is not unique');
    }
  }

  deleteStudentRow(row) {
    this.studentsData.splice(this.studentsData.indexOf(row.entity), 1);
    this.logicTableRow();
  };

  logicTableRow() {
    this.gridOptions.data = this.studentsData;
    this.data.find(res => res.id === this.activeTab).students = this.studentsData;
    this.calculateAverageGpa();
    if (this.studentName !== null) {
      this.checkNameStudent(this.studentName);
    }
    if (this.studentGpa !== null) {
      this.checkGpa(this.studentGpa);
    }
  }

  checkUniqueGrade(data) {
    this.uniqueGradeName = !this.data.find(res => res.gradeName === data) && data !== '';
  }

  checkNameStudent(data) {
    this.validNameStudent = this.nameRegex.test(data) && !this.studentsData.find(res => res.name === data);
  }

  checkGpa(data) {
    this.validGpa = this.floatRegex.test(data);
  }
}

SchoolGpaController.$inject = ['schoolGpaService'];
