export default class schoolGpaService {
  constructor($http) {
    this.$http = $http;
  }

  getLocalJson() {
    return this.$http.get('/app/components/schoolGpa/data.json');
  }
}

schoolGpaService.$inject = ['$http'];
