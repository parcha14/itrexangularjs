import angular from 'angular';
import dominoModule from './domino/domino.module';
import schoolGpaModule from './schoolGpa/schoolGpa.module';
import menubarModule from './menubar/menubar.module';

let componentModule = angular.module('app.components', [
  menubarModule,
  dominoModule,
  schoolGpaModule,
]).name;

export default componentModule;
