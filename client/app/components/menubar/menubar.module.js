import angular from 'angular';
import uiRouter from 'angular-ui-router';
import menubarComponent from './menubar.component';

let menubarModule = angular.module('menubar', [
  uiRouter
]).config(($stateProvider, $urlRouterProvider) => {
  "ngInject";
  $urlRouterProvider.otherwise('/');
  $stateProvider.state('menubar', {url: '/', component: 'menubar'});
}).component('menubar', menubarComponent).name;

export default menubarModule;
