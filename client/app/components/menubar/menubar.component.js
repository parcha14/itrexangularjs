import template from './menubar.template.html';
import controller from './menubar.controller';
import './menubar.style.css';

let menubarComponent = {
  bindings: {},
  template,
  controller
};

export default menubarComponent;
