import template from './domino.template.html';
import controller from './domino.controller';
import './domino.style.css';

let dominoComponent = {
  bindings: {},
  template,
  controller
};

export default dominoComponent;
