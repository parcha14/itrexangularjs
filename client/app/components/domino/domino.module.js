import angular from 'angular';
import uiRouter from 'angular-ui-router';
import dominoComponent from './domino.component';

let dominoModule = angular.module('domino', [
  uiRouter
]).config(($stateProvider) => {
  "ngInject";
  $stateProvider.state('domino', {url: '/domino', component: 'domino'});
}).component('domino', dominoComponent).name;

export default dominoModule;
